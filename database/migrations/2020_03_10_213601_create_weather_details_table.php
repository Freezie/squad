<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWeatherDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('weather_details', function (Blueprint $table) {
            $table->id();
            $table->string('lon')->default('36.821945');
            $table->string('lat')->default('-1.292066');
            $table->string('temp')->default('25.56');
            $table->string('pressure')->default('1015');
            $table->string('humidity')->default('61');
            $table->string('temp_min')->default('25.56');
            $table->string('temp_max')->default('25.56');
            $table->string('speed')->default('3.6');
            $table->string('deg')->default('230');
            $table->string('country')->default('KE');
            $table->string('sunrise')->default('1499369792');
            $table->string('sunset')->default('1499421666');
            $table->string('data_name')->default('1499421666');
            $table->string('data_cod')->default('1499421666');
            $table->string('data_id')->default('1499421666');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('weather_details');
    }
}
