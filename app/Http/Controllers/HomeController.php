<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\WeatherDetails;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        // dd($data);
        return view ('home');

    }
    public function getValues(Request $request){

        $lat=$request->get('lat');
        $long=$request->get('long');
        $base_url = 'https://fcc-weather-api.glitch.me//api/current';
        //accept the queery params for getting response.
        if(!empty($lat) && !empty($long)){
            //Get Data from Response:
            //Check for connection;
            try {
                $datafecth = $base_url.'?'.'lon'.'='.$long.'&'.'lat'.'='.$lat;
                $payload =  file_get_contents($datafecth);
                $data = json_decode($payload);
                $cord = $data->coord;
                $lon = $cord->lon;
                $lat = $cord->lat;
                $weather = $data->weather;
                $weather_main = $weather[0]->id;
                $temp = $data->main->temp;
                $temp_min = $data->main->temp_min;
                $temp_max = $data->main->temp_max;
                $pressure = $data->main->pressure;
                $humidity = $data->main->humidity;
                $country = $data->sys->country;
                $sunrise = $data->sys->sunrise;
                $sunset = $data->sys->sunset;
                $speed = $data->wind->speed;
                $deg = $data->wind->deg;
                $data_id = $data->id;
                $data_name = $data->name;
                $data_cod = $data->cod;
                //Save the Data to DB from the response
                $data = new WeatherDetails();
                $data->country = $country;
                $data->humidity = $humidity;
                $data->pressure = $pressure;
                $data->temp_max = $temp_max;
                $data->temp_min = $temp_min;
                $data->temp = $temp;
                $data->lat = $lat;
                $data->lon = $long;
                $data->speed = $speed;
                $data->deg = $deg;
                $data->sunrise = $sunrise;
                $data->sunset = $sunset;
                $data->data_id = $data_id;
                $data->data_name = $data_name;
                $data->data_cod = $data_cod;
                $data->save();

            } catch (\Exception $e) {
                // abort(404);
                $weather_items = WeatherDetails::all();
                $get_weather = $weather_items->where('lat',$lat)->where('lon',$long);
                // abort($e instanceof PDOException ? 503 : 500);
            }          
            //Fetch the data from the DB.
            $weather_items = WeatherDetails::all();
            $get_weather = $weather_items->where('lat',$lat)->where('lon',$long);
        }

        return view ('showweather',compact('get_weather'));

    }

}
