<?php

namespace App\Http\Controllers;

use App\WeatherDetails;
use Illuminate\Http\Request;

class WeatherDetailsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        // We check the internet connection from this point.
        $base_url = 'https://fcc-weather-api.glitch.me//api/current';
        //LatLong for Nairobi
        $lat = '-1.292066';
        $long = '36.821945';
        $datafecth = $base_url.'?'.'lon'.'='.$long.'&'.'lat'.'='.$lat;
        // $datafecth1 = 'https://fcc-weather-api.glitch.me//api/current?lon=36.821945&lat=-1.292066'; //this is for testing and mapping the parameter in the link
        if ($payload =  file_get_contents($datafecth)){

            // $payload = $payload.'?lon=36.821945&lat=-1.292066';
            // $payload =  file_get_contents('https://fcc-weather-api.glitch.me//api/current?lon=36.821945&lat=-1.292066');
            $data = json_decode($payload);
            $cord = $data->coord;
            $lon = $cord->lon;
            $lat = $cord->lat;
        //        dd($cord);
            $weather = $data->weather;
            $weather_main = $weather[0]->id;
            
            ///These data are under the main section.
            $temp = $data->main->temp;
            // $feels_like = $data->main->feels_like;
            $temp_min = $data->main->temp_min;
            $temp_max = $data->main->temp_max;
            $pressure = $data->main->pressure;
            $humidity = $data->main->humidity;
            //End of data under the main section.
            // dd($temp);
            // dd($data);

        }else{
            return $payload = false; 

        }
        
        // dd($datafecth1);
        return view ('weathercast', compact ('temp', 'temp_max', 'temp_min','pressure','humidity'));

       

        
    }

    public function weatherdata(){
        if ($data = @file_get_contents("https://www.geoip-db.com/json"))    
            {
                    $json = json_decode($data, true);
                    $country_code = $json['country_code'];
                    $latitude = $json['latitude'];
                    $longitude = $json['longitude'];
            } else {
                    $latitude = 0;
                    $longitude = 0;
                    $country_code = null;
            }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\WeatherDetails  $weatherDetails
     * @return \Illuminate\Http\Response
     */
    public function show(WeatherDetails $weatherDetails)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\WeatherDetails  $weatherDetails
     * @return \Illuminate\Http\Response
     */
    public function edit(WeatherDetails $weatherDetails)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\WeatherDetails  $weatherDetails
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, WeatherDetails $weatherDetails)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\WeatherDetails  $weatherDetails
     * @return \Illuminate\Http\Response
     */
    public function destroy(WeatherDetails $weatherDetails)
    {
        //
    }
}
