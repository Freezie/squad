@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Enter The Latitude and Longitude Get weather Data</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <form method="post" action="{{ action('HomeController@getValues') }}">
                            @csrf
                            <div class="form-group row">
                                <label for="lat" class="col-md-4 col-form-label text-md-right">{{ __('Latitute') }}</label>
                                <div class="col-md-6">
                                    <input id="lat" value='-1.292066' placeholder="Latitude: -1.292066" type="text" class="form-control" name="lat" required autocomplete="latitute" autofocus>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="long" class="col-md-4 col-form-label text-md-right">{{ __('Longitude') }}</label>
                                <div class="col-md-6">
                                    <input id="long" value='36.821945' type="text" placeholder="Longitude:36.821945" class="form-control" name="long" required autocomplete="longitude" autofocus>
                                </div>
                            </div>
                            <div class="form-group row mb-0">
                                <div class="col-md-8 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Search Weather Data') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
@endsection
