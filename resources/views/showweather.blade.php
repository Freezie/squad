@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">This is your weather details</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif  
                    </div>

                   

                   <h1>Weather information </h1>
                    <table class="table table-striped">
                        <tr>
                            <th><strong> Latitude: </strong></th>
                            <th><strong> Longitude: </strong></th>
                            <th><strong> Temparature: </strong></th>
                            <th><strong> Temparature Min: </strong></th>
                            <th><strong> Temparature Max: </strong></th>
                            <th><strong> Pressure: </strong></th>
                            <th><strong> Wind Speed: </strong></th>
                        </tr>                       

                        @forelse($get_weather as $weather_data)
                        <tr>
                            <td> {{$weather_data ->lat}} </td>
                            <td> {{$weather_data ->lon}}</td>
                            <td> {{$weather_data ->temp}}</td>
                            <td> {{$weather_data ->temp_min}}</td>
                            <td> {{$weather_data ->temp_max}}</td>
                            <td> {{$weather_data ->pressure}}</td>
                            <td> {{$weather_data ->speed}}</td>
                        </tr>
                        @empty
                        <tr>
                            <p>No Data Available</p>
                        </tr>
                        @endforelse
                    </table>
                   
                </div>
            </div>
        </div>
    </div>
@endsection
